# configuracion inicial
cat("\f")
rm(list = ls())

# load packages
if(!require(pacman)){install.packages(pacman) ; require(pacman)}
p_load(tidyverse,rio,here,sf,ggsn,ggpubr,rgdal,rgeos)

### Appi de goolge y tema
browseURL("https://developers.google.com/maps/documentation/maps-static/cloud-setup") # Get Api Key
register_google(key = "Api Key Here")

fig_a = ggmap(get_googlemap(center = c(lon = -74.04095351794948, lat = 4.681359149613566),
                    zoom = 16, scale = 2, maptype ='satellite', color = 'color')) + 
        labs(title="Figure A",x="",y="") + theme(plot.title=element_text(hjust=0.5,size=25))

fig_b = ggmap(get_googlemap(center = c(lon = -74.04095351794948, lat = 4.681359149613566),
                    zoom = 16, scale = 2, maptype ='terrain', color = 'color')) + 
        labs(title="Figure B",x="",y="") + theme(plot.title=element_text(hjust=0.5,size=25))

# save combine plot 
p = ggarrange(fig_a, fig_b, common.legend = TRUE, legend="bottom",widths = c(1,1))
p
ggsave(plot=p , filename="public/pics/gis_intro.png" , width=9 , height=4.5)

